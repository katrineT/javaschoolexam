package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        boolean result = false;

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        if (x.isEmpty()) {
            result = true;
        } else {
            int xIndex = 0;
            for (Object elem : y) {
                if (elem == x.get(xIndex)) {
                    xIndex++;
                }
                if (xIndex == x.size()) {
                    result = true;
                    break;
                }

            }
        }

        return result;
    }

}
