package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result = "";

        if (statement == null || !statement.matches("[-0-9.+*/()]+")) {
            return null;
        }
        statement = statement.replaceAll(" ", "");
        Stack<String> numbers = new Stack<>();
        Stack<String> operations = new Stack<>();

        List<String> tokens = getTokens(statement);
        if (!(String.join("", tokens).equals(statement) && checkTokens(tokens))) {
            return null;
        }


        for (String token : tokens) {
            if (token.matches("-?\\d+(?:\\.\\d+)?")) {
                numbers.push(token);
            } else if (token.equals("(")) {
                operations.push(token);
            } else if (token.equals(")")) {
                while (!operations.empty() && !operations.peek().equals("(") && !numbers.empty()) {
                    numbers.push(applyOperation(operations, numbers));
                }
                if (!(operations.empty() || numbers.empty())) {
                    operations.pop();
                } else {
                    result = null;
                    break;
                }
            } else {
                if (token.matches("[+-]")) {
                    while (!(operations.empty() || operations.peek().matches("[()]"))) {
                        numbers.push(applyOperation(operations, numbers));
                    }
                    operations.push(token);
                } else {
                    while (!(operations.empty() || operations.peek().matches("[-+()]"))) {
                        numbers.push(applyOperation(operations, numbers));
                    }
                    operations.push(token);
                }
            }
        }

        while (!operations.empty() && numbers.size() > 1) {
            numbers.push(applyOperation(operations, numbers));
        }
        if (operations.empty() && numbers.size() == 1 && result != null) {
            result = numbers.pop();
        } else {
            result = null;
        }

        return result != null && Double.parseDouble(result) == Math.floor(Double.parseDouble(result))
                ? String.valueOf(Math.round(Double.parseDouble(result))) : result;
    }

    private List<String> getTokens(String statement) {
        Matcher pattern = Pattern.compile("(\\()?(-?\\d+(?:\\.\\d+)?)([-+*)/])?([-+*(/])?").matcher(statement);

        List<String> tokens = new ArrayList<>();
        while (pattern.find()) {
            for (int i = 1; i <= pattern.groupCount(); i++) {
                if (pattern.group(i) != null) {
                    tokens.add(pattern.group(i));
                }
            }
        }
        return tokens;
    }

    private boolean checkTokens(List<String> tokens) {
        boolean result = true;
        String previousToken = tokens.get(0);
        for (int i = 1; i < tokens.size(); i++) {
            String current = tokens.get(i);
            if ((current.equals("(") && previousToken.equals(")"))
                    || (current.matches("[-+*/]") && previousToken.matches("[-+*/]"))) {
                result = false;
            }
            previousToken = current;
        }
        return result;
    }

    private String applyOperation(Stack<String> operations, Stack<String> numbers) {
        String operation = operations.pop();
        String rightOperand = numbers.pop();
        String leftOperand = numbers.pop();
        String result;
        switch (operation) {
            case "+":
                result = String.valueOf(Double.parseDouble(leftOperand) + Double.parseDouble(rightOperand));
                break;
            case "-":
                result = String.valueOf(Double.parseDouble(leftOperand) - Double.parseDouble(rightOperand));
                break;
            case "*":
                result = String.valueOf(Double.parseDouble(leftOperand) * Double.parseDouble(rightOperand));
                break;
            case "/":
                double temp = Double.parseDouble(leftOperand) / Double.parseDouble(rightOperand);
                result = Double.isFinite(temp) ? String.valueOf(temp) : null;
                break;
            default:
                result = "WRONG?!?";
        }
        return result;
    }

}
