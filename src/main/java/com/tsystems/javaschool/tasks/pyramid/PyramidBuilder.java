package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] result;

        if (inputNumbers == null || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        double countOfLevels = getCountOfLevels(inputNumbers.size());
        if (countOfLevels == Math.floor(countOfLevels) && countOfLevels >= 0) {
            List<Integer> sortedNumbers = new ArrayList<>(inputNumbers);
            sortedNumbers.sort(Comparator.naturalOrder());
            result = fillResult(countOfLevels, sortedNumbers);
        } else {
            throw new CannotBuildPyramidException();
        }

        return result;
    }

    private double getCountOfLevels(int size) {
        return size == 0 ? 0 : ((-1 + Math.sqrt(1 + 4 * size * 2)) / 2);
    }

    private int[][] fillResult(double countOfLevels, List<Integer> sortedNumbers) {
        int[][] result;

        int index = 0;
        int numberPosition = 0;
        int width = (int) (2 * countOfLevels - 1);
        int countOfSideZeros = (int) (countOfLevels - 1);
        result = new int[(int) countOfLevels][width];

        for (int level = 0; level < countOfLevels; level++) {
            for (int left = 0; left < countOfSideZeros; left++) {
                index = fillCell(result, index, width, 0);
            }

            for (int middle = 0; middle < (level + 1) + level; middle++) {
                if (middle % 2 == 0) {
                    index = fillCell(result, index, width, sortedNumbers.get(numberPosition));
                    numberPosition++;
                } else {
                    index = fillCell(result, index, width, 0);
                }
            }

            for (int right = 0; right < countOfSideZeros; right++) {
                index = fillCell(result, index, width, 0);
            }
            countOfSideZeros--;
        }
        return result;
    }

    private int fillCell(int[][] result, int index, int width, int i) {
        result[index / width][index % width] = i;
        index++;
        return index;
    }

}
